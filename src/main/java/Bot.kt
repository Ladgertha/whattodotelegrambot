package ru.ladgertha.whattodobot

import ru.ladgertha.whattodobot.entity.Result
import ru.ladgertha.whattodobot.network.GetUpdatesService
import ru.ladgertha.whattodobot.network.SendMessageApi

private const val URL = "https://api.telegram.org/botTOKEN/"
private const val TIME_OUT = 2000L

class Bot {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Thread {
                var result = getFirstMessage()
                var updateId = result?.last()?.updateId
                while (true) {
                    Thread.sleep(TIME_OUT)
                    result = GetUpdatesService(URL).getUpdates(updateId!!)
                    if (!result.isNullOrEmpty() && result.last().updateId > updateId) {
                        val msg = result.last().message.text
                        if (msg.isNotEmpty()) {
                            val answer = getBotAnswer(msg)
                            SendMessageApi(URL).sendMessage(result.last().message.chat.id, answer)
                            updateId = updateId.plus(1)
                        }
                    }
                }
            }.start()
        }

        private fun getFirstMessage(): List<Result>? {
            var result =
                    GetUpdatesService(URL).getUpdates()
            while (true) {
                if (result.isNullOrEmpty()) {
                    Thread.sleep(TIME_OUT)
                    result = GetUpdatesService(URL).getUpdates()
                } else {
                    break
                }
            }
            return result
        }

        private fun getBotAnswer(message: String): String {
            return with(message.toLowerCase()) {
                when {
                    contains("привет") -> "Привет!"
                    contains("как дела") || contains("как ты") -> "Хорошо"
                    contains("откуда ты") -> "Москва"
                    else -> "Я не знаю что мне ответить."
                }
            }
        }
    }
}