package ru.ladgertha.whattodobot.entity

import com.google.gson.annotations.SerializedName

data class Update(
    @SerializedName("ok")
    val ok: Boolean,
    @SerializedName("result")
    val result: List<Result>
)