package ru.ladgertha.whattodobot.entity


import com.google.gson.annotations.SerializedName

data class Chat(
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("type")
    val type: String,
    @SerializedName("username")
    val username: String
)