package ru.ladgertha.whattodobot.entity

import com.google.gson.annotations.SerializedName

data class From(
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_bot")
    val isBot: Boolean,
    @SerializedName("language_code")
    val languageCode: String,
    @SerializedName("username")
    val username: String
)