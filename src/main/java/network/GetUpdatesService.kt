package ru.ladgertha.whattodobot.network

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import ru.ladgertha.whattodobot.entity.Result
import ru.ladgertha.whattodobot.entity.Update
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Proxy
import okhttp3.*
import retrofit2.http.Query
import java.lang.Exception


class GetUpdatesService(
        private val url: String
) {

    fun getUpdates(id: Int = 0): List<Result>? {
        try {
            val call = if(id == 0) provideGetUpdatesService(url).getUpdates()
            else provideGetUpdatesService(url).getUpdatesWithId(id)
            val networkEntity = call.execute()
            when (networkEntity.code()) {
                RESPONSE_ERROR_CODE_FAILED -> {
                    throw Exception()
                }
                RESPONSE_CODE_OK -> {
                    return networkEntity.body()?.let {
                        return@let it.result
                    }
                }
                else -> {
                    throw Exception()
                }
            }
        } catch (ioException: IOException) {
            throw Exception(ioException)
        }
    }

    private fun provideGetUpdatesService(
            url: String
    ): GetUpdatesService {

        val proxy = Proxy(Proxy.Type.SOCKS, InetSocketAddress(HOST, PORT))
        val client = OkHttpClient.Builder().proxy(proxy).retryOnConnectionFailure(true).build()

        return Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GetUpdatesService::class.java)
    }


    private interface GetUpdatesService {

        @Headers("Content-Type: application/json")
        @GET("getUpdates")
        fun getUpdates(): Call<Update>

        @Headers("Content-Type: application/json")
        @GET("getUpdates")
        fun getUpdatesWithId(@Query("update_id") updateId: Int): Call<Update>
    }

    companion object {

        private const val RESPONSE_CODE_OK = 200
        private const val RESPONSE_ERROR_CODE_FAILED = 424
    }
}