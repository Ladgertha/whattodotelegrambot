package ru.ladgertha.whattodobot.network

import okhttp3.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Proxy

class SendMessageApi(
        private val url: String
) {

    fun sendMessage(chatId: Int, message: String) {
        try {
            val call = provideGetUpdatesService(url).sendMessage(chatId, message)
            val networkEntity = call.execute()
            when (networkEntity.code()) {
                RESPONSE_ERROR_CODE_FAILED -> {
                    throw Exception()
                }
                RESPONSE_CODE_OK -> {

                }
                else -> {
                    throw Exception()
                }
            }
        } catch (ioException: IOException) {
            throw Exception(ioException)
        }
    }

    private fun provideGetUpdatesService(
            url: String
    ): SendMessageService {
        val proxy = Proxy(Proxy.Type.SOCKS, InetSocketAddress(HOST, PORT))
        val client = OkHttpClient.Builder().proxy(proxy).retryOnConnectionFailure(true).build()

        return Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SendMessageService::class.java)
    }


    private interface SendMessageService {

        @Headers("Content-Type: application/json")
        @GET("sendMessage")
        fun sendMessage(@Query("chat_id") chatId: Int, @Query("text") message: String): Call<Any>
    }

    companion object {

        private const val RESPONSE_CODE_OK = 200
        private const val RESPONSE_ERROR_CODE_FAILED = 424
    }
}